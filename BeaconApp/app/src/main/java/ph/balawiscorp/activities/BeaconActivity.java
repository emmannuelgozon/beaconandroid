package ph.balawiscorp.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ph.balawiscorp.adapters.BeaconAdapter;
import ph.balawiscorp.R;
import ph.balawiscorp.models.Client;
import ph.balawiscorp.models.Login;
import ph.balawiscorp.models.response.LoginResponse;
import ph.balawiscorp.service.impl.BeaconAPIImpl;

public class BeaconActivity extends Activity implements BeaconConsumer,
        Response.Listener<LoginResponse>, Response.ErrorListener {

    private static final String TAG = BeaconActivity.class.getSimpleName();
    private static final String API_URL = "http://tekcopia.com/sitemapper/login";
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final String BEACON_LAYOUT[] = {
            "m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25",
            // Detect the main identifier (UID) frame:
            "s:0-1=feaa,m:2-2=00,p:3-3:-41,i:4-13,i:14-19",
            // Detect the telemetry (TLM) frame:
            "x,s:0-1=feaa,m:2-2=20,d:3-3,d:4-5,d:6-7,d:8-11,d:12-15",
            // Detect the URL frame:
            "s:0-1=feaa,m:2-2=10,p:3-3:-41,i:4-20v",
            "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"
    };

    private BeaconManager beaconManager;

    private ListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listview = (ListView) findViewById(R.id.listview);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Login login = new Login();
                login.setUsername("Q");
                login.setPassword("q");

                BeaconAPIImpl.getInstance(BeaconActivity.this).login(login, BeaconActivity.this,
                        BeaconActivity.this);
            }
        });

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            finish();
        }

        final BluetoothAdapter bluetoothAdapater = BluetoothAdapter.getDefaultAdapter();
        if (null == bluetoothAdapater) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.bluetooth_not_found);
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
            builder.show();

        } else {
            if (!bluetoothAdapater.isEnabled()) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.bluetooth_message);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        bluetoothAdapater.enable();
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
                builder.show();
            }
        }

        if (ContextCompat.checkSelfPermission(BeaconActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                displayNeedPermissionDialog();
            }
        } else {
            initialize();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, getString(R.string.loc_permission_granted));
                    initialize();
                    bindBeacon();
                } else {
                    Log.d(TAG, getString(R.string.loc_permission_denied));
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(getString(R.string.loc_permission_msg));
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }
            }
        }
    }

    private void initialize() {
        beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.setForegroundScanPeriod(5000);
        beaconManager.getBeaconParsers().clear();
        for (String beaconLayout : BEACON_LAYOUT) {
            BeaconParser bp = new BeaconParser();
            bp.setBeaconLayout(beaconLayout);
            beaconManager.getBeaconParsers().add(bp);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.setMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {
                String msg = "Beacon found at region: " + region.getUniqueId();
                Log.i(TAG, msg);
                try {
                    beaconManager.startRangingBeaconsInRegion(region);
                } catch (RemoteException e) {
                    msg = e.getMessage();
                    Log.d(TAG, msg);
                }
            }

            @Override
            public void didExitRegion(Region region) {
                String msg = "Beacon not found at region: " + region.getUniqueId();
                Log.i(TAG, msg);
            }

            @Override
            public void didDetermineStateForRegion(int i, Region region) {
                String msg = "Beacons detected at region " + region.getUniqueId() + ": " + i;
                Log.i(TAG, msg);
            }
        });


        beaconManager.setRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                final List<Beacon> beaconList = new ArrayList<>(beacons);
                beaconList.size();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listview.setAdapter(new BeaconAdapter(BeaconActivity.this, beaconList));
                    }
                });

            }
        });

        try {
            beaconManager.startRangingBeaconsInRegion(new Region(Beacon.class.getSimpleName(),
                    null, null, null));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindBeacon();
    }


    @Override
    protected void onPause() {
        super.onPause();
        unBindBeacon();
    }

    private void displayNeedPermissionDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.loc_message);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                ActivityCompat.requestPermissions(BeaconActivity.this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSION_REQUEST_COARSE_LOCATION);
            }
        });
        builder.show();
    }


    private void bindBeacon() {
        if (null != beaconManager && !beaconManager.isBound(this)) {
            beaconManager.bind(this);
        }
    }

    private void unBindBeacon() {
        if (null != beaconManager && beaconManager.isBound(this)) {
            beaconManager.unbind(this);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e(TAG, error.getMessage());
    }

    @Override
    public void onResponse(LoginResponse response) {
        Log.d(TAG, "error: " + response.isError());
        if(!response.isError()){
            Client client = response.getClient();
            String id = client.getFldClientID();
            String firstName = client.getFldClientFirstname();
            String lastName = client.getFldClientLastname();
            String email = client.getFldClientEmail();
            String username = client.getFldClientUsername();


            Log.d(TAG, "id: " + id);
            Log.d(TAG, "firstName: " + firstName);
            Log.d(TAG, "lastName: " + lastName);
            Log.d(TAG, "email: " + email);
            Log.d(TAG, "username: " + username);

        }
        else{

            Log.d(TAG, "message: " + response.getMessage());
        }
    }
}
