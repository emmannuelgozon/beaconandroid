package ph.balawiscorp.models;

/**
 * Created by Charles Vera Cruz on 25/05/2016.
 */
public class Client {

    private String fldClientID;
    private String fldClientFirstname;
    private String fldClientLastname;
    private String fldClientEmail;
    private String fldClientUsername;

    public String getFldClientID() {
        return fldClientID;
    }

    public void setFldClientID(String fldClientID) {
        this.fldClientID = fldClientID;
    }

    public String getFldClientFirstname() {
        return fldClientFirstname;
    }

    public void setFldClientFirstname(String fldClientFirstname) {
        this.fldClientFirstname = fldClientFirstname;
    }

    public String getFldClientLastname() {
        return fldClientLastname;
    }

    public void setFldClientLastname(String fldClientLastname) {
        this.fldClientLastname = fldClientLastname;
    }

    public String getFldClientEmail() {
        return fldClientEmail;
    }

    public void setFldClientEmail(String fldClientEmail) {
        this.fldClientEmail = fldClientEmail;
    }

    public String getFldClientUsername() {
        return fldClientUsername;
    }

    public void setFldClientUsername(String fldClientUsername) {
        this.fldClientUsername = fldClientUsername;
    }
}
