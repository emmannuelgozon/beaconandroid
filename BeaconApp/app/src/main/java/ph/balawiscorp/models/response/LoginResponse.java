package ph.balawiscorp.models.response;

import ph.balawiscorp.models.Client;

/**
 * Created by Charles Vera Cruz on 25/05/2016.
 */
public class LoginResponse {

    boolean error;
    String message;
    Client client;


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
