package ph.balawiscorp.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.altbeacon.beacon.Beacon;

import java.util.List;

import ph.balawiscorp.R;

/**
 * Created by Charles Vera Cruz on 24/05/2016.
 */
public class BeaconAdapter extends BaseAdapter {

    private static final String TAG = BeaconAdapter.class.getSimpleName();
    private static final String ICON_URL = "http://tekcopia.com/sitemapper/upload/clients/40/2016-05-23_06:42:31.png";

    private Context context;
    private List<Beacon> beacons;

    public BeaconAdapter(Context context, List<Beacon> beacons) {
        this.context = context;
        this.beacons = beacons;
    }

    @Override
    public int getCount() {
        return beacons.size();
    }

    @Override
    public Object getItem(int position) {
        return beacons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Integer.valueOf(beacons.get(position).getId2().toString());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.beacon_item, null);

            vh = new ViewHolder();
            vh.icon = (ImageView) convertView.findViewById(R.id.icon);
            vh.id = (TextView) convertView.findViewById(R.id.uid);
            vh.major = (TextView) convertView.findViewById(R.id.major);
            vh.minor = (TextView) convertView.findViewById(R.id.minor);
            vh.location = (TextView) convertView.findViewById(R.id.location);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        Beacon b = beacons.get(position);
        Log.i(TAG, b.toString());

        Picasso.with(context).load(ICON_URL).into(vh.icon);
        vh.id.setText(b.getId1().toString());
        vh.major.setText(b.getId2().toString());
        vh.minor.setText(b.getId3().toString());
        vh.location.setText(String.valueOf(b.getDistance()));

        return convertView;
    }

    class ViewHolder {
        ImageView icon;
        TextView id;
        TextView major;
        TextView minor;
        TextView location;
    }
}
