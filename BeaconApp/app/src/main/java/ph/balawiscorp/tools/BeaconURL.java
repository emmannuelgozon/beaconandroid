package ph.balawiscorp.tools;

import android.net.Uri;

/**
 * Created by Charles Vera Cruz on 27/04/2016.
 */
public class BeaconURL {
    // hostname
    //http://tekcopia.com/sitemapper/api/v1/login
    private static final String SCHEME = "http";
    private static final String AUTHORITY = "tekcopia.com";
    private static final String SITEMAPPER = "sitemapper";
    private static final String API = "api";
    private static final String VERSION = "v1";

    private static final String LOGIN = "login";


    private static final Uri BASE_URI = new Uri.Builder()
            .scheme(SCHEME)
            .authority(AUTHORITY)
            .appendPath(SITEMAPPER)
            .appendPath(API)
            .appendPath(VERSION).build();

    public static class ComposedURI {
        public static final Uri LOGIN_URL =
                BASE_URI.buildUpon().appendPath(LOGIN).build();
    }
}
