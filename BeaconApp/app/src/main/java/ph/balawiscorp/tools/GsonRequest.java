package ph.balawiscorp.tools;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * <pre>
 *     <b>GsonRequest</b>
 *     Request for Volley transactions. Returns a deserialzed Json object using Gson
 *     @param <T> Type of Response.
 * </pre>
 */
public class GsonRequest<T> extends Request<T> {
    private static final String BODY_CONTENT_TYPE = "application/x-www-form-urlencoded";
    private static final String PROTOCOL_CHARSET = "utf-8";

    private final Gson gson = new Gson();
    private final Class<T> mClass;
    private final Map<String, String> headers;
    private final Response.Listener<T> listener;
    private final Map<String, String> params;
    private final String requestBody;

    /**
     * <pre>
     * Make a GET request and return a parsed object from JSON.
     *
     * @param method
     *  int DEPRECATED_GET_OR_POST = -1;
     *  int GET = 0;
     *  int POST = 1;
     *  int PUT = 2;
     *  int DELETE = 3;
     *  int HEAD = 4;
     *  int OPTIONS = 5;
     *  int TRACE = 6;
     *  int PATCH = 7;
     * @param url URL of the request to make
     * @param mClass Relevant class object, for Gson's reflection
     * @param headers Map of request headers
     * @param params Parameters for Post methods. Use null for GET Requests
     *
     * </pre>
     */
    public GsonRequest(int method, String url, String requestBody, Class<T> mClass,
                       Map<String, String> headers, Response.Listener<T> listener,
                       Response.ErrorListener errorListener, Map<String, String> params) {
        super(method, url, errorListener);
        this.mClass = mClass;
        this.headers = headers;
        this.listener = listener;
        this.params = params;
        this.requestBody = requestBody;
    }



    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    public String getBodyContentType() {
        return BODY_CONTENT_TYPE + "; charset=" + getParamsEncoding();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(
                    gson.fromJson(json, mClass),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    public byte[] getBody() {
        try {
            return requestBody == null ? null : requestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    requestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params;
    }
}