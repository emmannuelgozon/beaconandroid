package ph.balawiscorp.tools;

import ph.balawiscorp.models.Login;

/**
 * <pre>
 *     <b>URLEncodedHelper</b>
 *     Helper class for application/x-www-form-urlencoded requests
 * </pre>
 */
public class URLEncodedHelper {

    private static final String AND = "&";

    /* For Login */
    private static final String USERNAME = "username=";
    private static final String PASSWORD = "password=";

    @SuppressWarnings("unchecked")
    public static String createLoginUserRequestBody(Login login) {
        return new StringBuilder()
                .append(USERNAME)
                .append(login.getUsername())
                .append(AND)
                .append(PASSWORD)
                .append(login.getPassword())
                .toString();

    }
}
