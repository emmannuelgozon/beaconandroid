package ph.balawiscorp.service.impl;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import ph.balawiscorp.models.Login;
import ph.balawiscorp.models.response.LoginResponse;
import ph.balawiscorp.service.BeaconAPI;
import ph.balawiscorp.tools.BeaconURL;
import ph.balawiscorp.tools.GsonRequest;
import ph.balawiscorp.tools.URLEncodedHelper;

/**
 * Created by Charles Vera Cruz on 25/05/2016.
 */
public class BeaconAPIImpl implements BeaconAPI {

    private static final String TAG = BeaconAPIImpl.class.getSimpleName();
    private static final int TIMEOUT_MS = 10000;
    private static BeaconAPIImpl instance;
    private static Context context;
    private Gson gson;

    private RequestQueue requestQueue;

    private BeaconAPIImpl(Context context) {
        this.context = context;
        this.requestQueue = getRequestQueue();
        this.gson = new Gson();
    }

    public static synchronized BeaconAPIImpl getInstance(Context context) {
        if (null == instance) {
            instance = new BeaconAPIImpl(context);
        }
        return instance;
    }

    /**
     * Returns a RequestQueue object. RequestQueue should always be created with the
     * application context
     *
     * @return Updated RequestQueue
     */
    public RequestQueue getRequestQueue() {
        if (null == requestQueue) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

    /**
     * Adds a request to the requestQueue
     *
     * @param request The Request to be added
     * @param <T>     Type of Request
     */
    public <T> void addToRequestQueue(Request<T> request) {
        getRequestQueue().add(request);
    }

    @Override
    public void login(Login login, Response.Listener<LoginResponse> successListener, Response.ErrorListener errorListener) {
        GsonRequest<LoginResponse> request = new GsonRequest<LoginResponse>(Request.Method.POST,
                BeaconURL.ComposedURI.LOGIN_URL.toString(), URLEncodedHelper.createLoginUserRequestBody(login),
                LoginResponse.class, null, successListener, errorListener, null);
        addToRequestQueue(request);
    }
}
