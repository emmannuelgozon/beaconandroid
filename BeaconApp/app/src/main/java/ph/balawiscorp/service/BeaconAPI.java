package ph.balawiscorp.service;

import com.android.volley.Response;

import ph.balawiscorp.models.Login;
import ph.balawiscorp.models.response.LoginResponse;

/**
 * Created by Charles Vera Cruz on 25/05/2016.
 */
public interface BeaconAPI {

    void login(Login login, Response.Listener<LoginResponse> successListener,
               Response.ErrorListener errorListener);
}
